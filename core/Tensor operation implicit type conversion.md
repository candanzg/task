[toc]

<a name="home"/>

- [1.pytorch1.5 Tensor运算，隐式类型转换](#1)
  - [1.1 Tensor与Tensor运算，隐式类型转换：](#1.1)
  - [1.2 Tensor与scalar运算，隐式类型转换](#1.2)
- [2.TensorFlow2.2 Tensor运算隐式类型转换](#2)
  - [2.1 Tensor与Tensor运算，隐式类型转换](#2.1)
  - [2.2 Tensor与Scalar运算，隐式类型转换](#2.2)
- [3. MindSpore Tensor运算隐式类型转换](#3)
  - [3.1 隐式类型转换支持数据类型](#3.1)
  - [3.2 隐式类型转换规则](#3.2)
    - [3.2.1 Tensor与Tensor运算](#3.2.1)
    - [3.2.2 Tensor与Scalar运算](#3.2.2)

# Tensor运算 隐式类型转换方案

## 1.pytorch1.5 Tensor运算，隐式类型转换    <a name="1" href="#home" >返回目录</a>

![image-20200514083917181](img/image-20200514083917181.png)

```python
int8_ = torch.arange(6).reshape(2,3).type(torch.int8)
int16_ = torch.arange(6).reshape(2,3).type(torch.int16)
int32_ = torch.arange(6).reshape(2,3).type(torch.int32)
int64_ = torch.arange(6).reshape(2,3).type(torch.int64)
uint8_ = torch.arange(6).reshape(2,3).type(torch.uint8)
float16_ = torch.arange(6).reshape(2,3).type(torch.float16)
float32_ = torch.arange(6).reshape(2,3).type(torch.float32)
float64_ = torch.arange(6).reshape(2,3).type(torch.float64)
```

### 1.1 Tensor与Tensor运算，隐式类型转换：  <a name="1.1" href="#home" >返回目录</a>

Int8->int16->int32->int64->float16->float32->float64

Uint8->.......

1. 按照此顺序隐式转换：Int8->int16->int32->int64->float16->float32->float64
2. Tensor(int8) + Tensor(uint8) -> int16
3. Tensor(fp16), 不能与非float的Tensor 或 scalar 运算.

```python
>>> (int8_ + int16_).dtype
torch.int16
>>> (uint8_ + int32_).dtype
torch.int32
>>> (int64_ + float32_).dtype
torch.float32
>>> (int64_ + float64_).dtype
torch.float64
>>> (int64_ + float16_).dtype
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
RuntimeError: "add_cpu/sub_cpu" not implemented for 'Half'
>>> (int16_ + float16_).dtype
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
RuntimeError: "add_cpu/sub_cpu" not implemented for 'Half'
>>> (int8_ + float16_).dtype
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
RuntimeError: "add_cpu/sub_cpu" not implemented for 'Half'
>>> (uint8_ + float16_).dtype
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
RuntimeError: "add_cpu/sub_cpu" not implemented for 'Half'
>>> (uint8_ + int8_).dtype
torch.int16
>>> (uint8_ + int16_).dtype
torch.int16
```

### 1.2 Tensor与scalar运算，隐式类型转换    <a name="1.2" href="#home" >返回目录</a>

1.Tensor(int/uint) + Scalar(int)  -> 类型向Tensor看齐

2.Tensor(int/uint) + Scalar(float) -> 类型为float32

3.Tensor(fp) + Scalar(float) -> 两者中最大类型

```python
>>> (uint8_ + 1111111111111).dtype
torch.uint8
>>> (uint8_ + 1111111111111222222).dtype
torch.uint8
>>> (int16_ + 1111111111111222222).dtype
torch.int16
>>> (int64_ + 1111111111111222222).dtype
torch.int64
>>> (int64_ + 1.111111111111222222).dtype
torch.float32
>>> (int8_ + 1.111111111111222222).dtype
torch.float32
>>> (uint8_ + 1.111111111111222222).dtype
torch.float32
>>> (float16_ + 1.111111111111222222).dtype
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
RuntimeError: "add_cpu/sub_cpu" not implemented for 'Half'
>>> (float32_ + 1.111111111111222222).dtype
torch.float32
>>> (float64_ + 1.111111111111222222).dtype
torch.float64
>>> (float16_ + 1).dtype
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
RuntimeError: "add_cpu/sub_cpu" not implemented for 'Half'
```

## 2.TensorFlow2.2 Tensor运算隐式类型转换    <a name="2" href="#home" >返回目录</a>

![image-20200514084500102](img/image-20200514084500102.png)

```
import numpy as np
a = np.arange(6).reshape(2,3)

int8_ = tf.constant(a,dtype=tf.int8)
int16_ = tf.constant(a,dtype=tf.int16)
int32_ = tf.constant(a,dtype=tf.int32)
int64_ = tf.constant(a,dtype=tf.int64)
uint8_ = tf.constant(a,dtype=tf.uint8)
uint16_ = tf.constant(a,dtype=tf.uint16)
uint32_ = tf.constant(a,dtype=tf.uint32)
uint64_ = tf.constant(a,dtype=tf.uint64)

float16_ = tf.constant(a,dtype=tf.float16)
float32_ = tf.constant(a,dtype=tf.float32)
float64_ = tf.constant(a,dtype=tf.float64)

```

### 2.1 Tensor与Tensor运算，隐式类型转换    <a name="2.1" href="#home" >返回目录</a>

两个不同dtype的Tensor运算，不支持隐式类型转换，报错。

```python

>>> (float32_ + float64_)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "/root/miniconda3/lib/python3.7/site-packages/tensorflow_core/python/ops/math_ops.py", line 902, in binary_op_wrapper
    return func(x, y, name=name)
  File "/root/miniconda3/lib/python3.7/site-packages/tensorflow_core/python/ops/math_ops.py", line 1194, in _add_dispatch
    return gen_math_ops.add_v2(x, y, name=name)
  File "/root/miniconda3/lib/python3.7/site-packages/tensorflow_core/python/ops/gen_math_ops.py", line 480, in add_v2
    _ops.raise_from_not_ok_status(e, name)
  File "/root/miniconda3/lib/python3.7/site-packages/tensorflow_core/python/framework/ops.py", line 6606, in raise_from_not_ok_status
    six.raise_from(core._status_to_exception(e.code, message), None)
  File "<string>", line 3, in raise_from
tensorflow.python.framework.errors_impl.InvalidArgumentError: cannot compute AddV2 as input #1(zero-based) was expected to be a float tensor but is a double tensor [Op:AddV2] name: add/

>>> (int32_ + int16_)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "/root/miniconda3/lib/python3.7/site-packages/tensorflow_core/python/ops/math_ops.py", line 902, in binary_op_wrapper
    return func(x, y, name=name)
  File "/root/miniconda3/lib/python3.7/site-packages/tensorflow_core/python/ops/math_ops.py", line 1194, in _add_dispatch
    return gen_math_ops.add_v2(x, y, name=name)
  File "/root/miniconda3/lib/python3.7/site-packages/tensorflow_core/python/ops/gen_math_ops.py", line 480, in add_v2
    _ops.raise_from_not_ok_status(e, name)
  File "/root/miniconda3/lib/python3.7/site-packages/tensorflow_core/python/framework/ops.py", line 6606, in raise_from_not_ok_status
    six.raise_from(core._status_to_exception(e.code, message), None)
  File "<string>", line 3, in raise_from
tensorflow.python.framework.errors_impl.InvalidArgumentError: cannot compute AddV2 as input #1(zero-based) was expected to be a int32 tensor but is a int16 tensor [Op:AddV2] name: add/

>>> (uint8_ + uint16_)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "/root/miniconda3/lib/python3.7/site-packages/tensorflow_core/python/ops/math_ops.py", line 902, in binary_op_wrapper
    return func(x, y, name=name)
  File "/root/miniconda3/lib/python3.7/site-packages/tensorflow_core/python/ops/math_ops.py", line 1194, in _add_dispatch
    return gen_math_ops.add_v2(x, y, name=name)
  File "/root/miniconda3/lib/python3.7/site-packages/tensorflow_core/python/ops/gen_math_ops.py", line 480, in add_v2
    _ops.raise_from_not_ok_status(e, name)
  File "/root/miniconda3/lib/python3.7/site-packages/tensorflow_core/python/framework/ops.py", line 6606, in raise_from_not_ok_status
    six.raise_from(core._status_to_exception(e.code, message), None)
  File "<string>", line 3, in raise_from
tensorflow.python.framework.errors_impl.InvalidArgumentError: cannot compute AddV2 as input #1(zero-based) was expected to be a uint8 tensor but is a uint16 tensor [Op:AddV2] name: add/

```

### 2.2 Tensor与Scalar运算，隐式类型转换    <a name="2.2" href="#home" >返回目录</a>

1.Tensor(unit8)  只能与Scalar(int)运算，类型向Tensor看齐（uint16/uint32/uint64 不支持隐式转换， Tensor(uint8) 不能与 Scalar(fp)运算)

2. Tensor(int8/16/32/64) 只能 与Scalar(int)运算。类型向Tensor看齐。
3. Tensor(fp16/32/64) + Scalar(int/fp)，类型向Tensor看齐

```python
>>> (uint8_ + 2).dtype
tf.uint8
>>> (uint16_ + 2).dtype
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "/root/miniconda3/lib/python3.7/site-packages/tensorflow_core/python/ops/math_ops.py", line 915, in binary_op_wrapper
    return func(x, y, name=name)
  File "/root/miniconda3/lib/python3.7/site-packages/tensorflow_core/python/ops/math_ops.py", line 1194, in _add_dispatch
    return gen_math_ops.add_v2(x, y, name=name)
  File "/root/miniconda3/lib/python3.7/site-packages/tensorflow_core/python/ops/gen_math_ops.py", line 480, in add_v2
    _ops.raise_from_not_ok_status(e, name)
  File "/root/miniconda3/lib/python3.7/site-packages/tensorflow_core/python/framework/ops.py", line 6606, in raise_from_not_ok_status
    six.raise_from(core._status_to_exception(e.code, message), None)
  File "<string>", line 3, in raise_from
tensorflow.python.framework.errors_impl.NotFoundError: Could not find valid device for node.
Node:{{node AddV2}}
All kernels registered for op AddV2 :
  device='XLA_CPU'; T in [DT_FLOAT, DT_DOUBLE, DT_INT32, DT_UINT8, DT_INT16, DT_INT8, DT_COMPLEX64, DT_INT64, DT_BFLOAT16, DT_COMPLEX128, DT_HALF]
  device='XLA_CPU_JIT'; T in [DT_FLOAT, DT_DOUBLE, DT_INT32, DT_UINT8, DT_INT16, DT_INT8, DT_COMPLEX64, DT_INT64, DT_BFLOAT16, DT_COMPLEX128, DT_HALF]
  device='CPU'; T in [DT_COMPLEX128]
  device='CPU'; T in [DT_UINT8]
  device='CPU'; T in [DT_COMPLEX64]
  device='CPU'; T in [DT_INT16]
  device='CPU'; T in [DT_INT8]
  device='CPU'; T in [DT_BFLOAT16]
  device='CPU'; T in [DT_INT64]
  device='CPU'; T in [DT_INT32]
  device='CPU'; T in [DT_DOUBLE]
  device='CPU'; T in [DT_HALF]
  device='CPU'; T in [DT_FLOAT]
 [Op:AddV2] name: add/
>>> (uint8_ + 2.0).dtype
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "/root/miniconda3/lib/python3.7/site-packages/tensorflow_core/python/ops/math_ops.py", line 915, in binary_op_wrapper
    return func(x, y, name=name)
  File "/root/miniconda3/lib/python3.7/site-packages/tensorflow_core/python/ops/math_ops.py", line 1194, in _add_dispatch
    return gen_math_ops.add_v2(x, y, name=name)
  File "/root/miniconda3/lib/python3.7/site-packages/tensorflow_core/python/ops/gen_math_ops.py", line 480, in add_v2
    _ops.raise_from_not_ok_status(e, name)
  File "/root/miniconda3/lib/python3.7/site-packages/tensorflow_core/python/framework/ops.py", line 6606, in raise_from_not_ok_status
    six.raise_from(core._status_to_exception(e.code, message), None)
  File "<string>", line 3, in raise_from
tensorflow.python.framework.errors_impl.InvalidArgumentError: cannot compute AddV2 as input #1(zero-based) was expected to be a uint8 tensor but is a float tensor [Op:AddV2] name: add/
>>>
>>> (int8_ + 2).dtype
tf.int8
>>> (int16_ + 2).dtype
tf.int16
>>> (int64_ + 2).dtype
tf.int64
>>> (int8_ + 2.0).dtype
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "/root/miniconda3/lib/python3.7/site-packages/tensorflow_core/python/ops/math_ops.py", line 915, in binary_op_wrapper
    return func(x, y, name=name)
  File "/root/miniconda3/lib/python3.7/site-packages/tensorflow_core/python/ops/math_ops.py", line 1194, in _add_dispatch
    return gen_math_ops.add_v2(x, y, name=name)
  File "/root/miniconda3/lib/python3.7/site-packages/tensorflow_core/python/ops/gen_math_ops.py", line 480, in add_v2
    _ops.raise_from_not_ok_status(e, name)
  File "/root/miniconda3/lib/python3.7/site-packages/tensorflow_core/python/framework/ops.py", line 6606, in raise_from_not_ok_status
    six.raise_from(core._status_to_exception(e.code, message), None)
  File "<string>", line 3, in raise_from
tensorflow.python.framework.errors_impl.InvalidArgumentError: cannot compute AddV2 as input #1(zero-based) was expected to be a int8 tensor but is a float tensor [Op:AddV2] name: add/
>>> (int64_ + 2.0).dtype
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "/root/miniconda3/lib/python3.7/site-packages/tensorflow_core/python/ops/math_ops.py", line 915, in binary_op_wrapper
    return func(x, y, name=name)
  File "/root/miniconda3/lib/python3.7/site-packages/tensorflow_core/python/ops/math_ops.py", line 1194, in _add_dispatch
    return gen_math_ops.add_v2(x, y, name=name)
  File "/root/miniconda3/lib/python3.7/site-packages/tensorflow_core/python/ops/gen_math_ops.py", line 480, in add_v2
    _ops.raise_from_not_ok_status(e, name)
  File "/root/miniconda3/lib/python3.7/site-packages/tensorflow_core/python/framework/ops.py", line 6606, in raise_from_not_ok_status
    six.raise_from(core._status_to_exception(e.code, message), None)
  File "<string>", line 3, in raise_from
tensorflow.python.framework.errors_impl.InvalidArgumentError: cannot compute AddV2 as input #1(zero-based) was expected to be a int64 tensor but is a float tensor [Op:AddV2] name: add/

 >>> (float16_ + 2.0).dtype
tf.float16
>>> (float64_ + 2.0).dtype
tf.float64
>>> (float64_ + 2).dtype
tf.float64
>>> (float64_ + 1).dtype
tf.float64
>>> (float16_ + 1).dtype
tf.float16
```



## 3. MindSpore Tensor运算隐式类型转换    <a name="3" href="#home" >返回目录</a>

已有方案：Tensor 与 scalar 运算，类型向Tensor看齐

建议方案：

### 3.1 隐式类型转换支持数据类型    <a name="3.1" href="#home" >返回目录 </a>

```
uint8
int8
int16
int32
int64
float16
float32
float64
不在此列类型，不做自动类型转换
```

### 3.2 隐式类型转换规则    <a name="3.2" href="#home" >返回目录</a>

#### 3.2.1 Tensor与Tensor运算<a name="3.2.1"/>

1.遵循：Int8->int16->int32->int64->float16->float32->float64

2.Tensor(int8) 与 Tensor(uint8) -> Tensor(int16)

#### 3.2.2 Tensor与Scalar运算<a name="3.2.2"/>

1.Tensor(int/uint8) + Scalar(int)  -> 类型向Tensor看齐

2.Tensor(int/uint) + Scalar(float) -> 类型为float32

3.Tensor(fp) + Scalar(float) -> 两者中最大类型

另外：signature中“sig_rw.RW_WRITE” 参数，参与类型的推导，但是不执行Cast操作。如果推导出的类型与“sig_rw.RW_WRITE”的类型不匹配，抛出异常。例如Assign、AssignSub等

```python
   __mindspore_signature__ = (
        ('input_x', sig_rw.RW_WRITE, sig_kind.KIND_POSITIONAL_KEYWORD, sig_kind.KIND_EMPTY_DEFAULT_VALUE, sig_dtype.T),
        ('indices', sig_rw.RW_READ, sig_kind.KIND_POSITIONAL_KEYWORD, sig_kind.KIND_EMPTY_DEFAULT_VALUE, sig_dtype.T1),
        ('value', sig_rw.RW_READ, sig_kind.KIND_POSITIONAL_KEYWORD, sig_kind.KIND_EMPTY_DEFAULT_VALUE, sig_dtype.T)
    )

```

