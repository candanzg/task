summary的主要工作包括python端和c++端，其中python的工作由我承接给MindData（干系人：寇振中、温凯、欧功畅），c++部分之前主要是有王秋亮看护。

python侧的主要功能是在缓存中拿数据记录到event日志中。以下是主要环节的介绍。

1. Summary 功能概述

   Summary的作用，这里不赘述了。主要功能分两类，一类是GraphSummary，另一类是其他的Summary功能（可以简单称为Summary算子）。

   GraphSummary 主要是用来记录编译生成图，也就是不一定启动训练，就可以保存。其他Summary算子类都是在启动训练或推理时才记录数据。

2、Summary目录结构

train/summary/

​						summary_record.py 这是唯一对外接口文件，只有对外接口是 SummaryRecord 类的record flush close

​						_summary_adapter.py 之所以叫adapter，设计之初，是想让它既支持tf又支持ms，但是现在只需要支持ms就可以。这个文件的主要作用，是解析、校验不同summary的数据，并将它们封装到event里。

​						_summary_schedule.py 	主要是 summary 并发封装，不影响训练。实际使用过程中，发现可能有一些不足之处：1) 可能训练完了，但summary还没有保存完成，因此进程要等待一会儿，才能推出。2）可能需要个线程池约束一下并发总量。

​						_event_writer.py 主要event文件写操作的封装，其继承于C++端的类。

3、主要功能

3.1 summary.record

![image-20200513102556341](img/image-20200513102556341.png)

150~163 主要是用来记录Graph的，也就是GraphSummary实际起作用的时刻，当第一次调用record时会执行此次。

165~177 是在缓存中拿到数据，然后并发去处理，这里需注意：缓存中拿到的不一定是一次step的数据。这里抓住一点就可以：自始至终，不能有数据丢失。

3.2 summary_adapter

1.summary支持的功能有可以从 次文件中得知，如下图所示。同时也展示了 summary如何解析和封装对象的。每个summary算子的支持，这里就不赘述了，看源码即可了解，同时可以参考tf的实现。

![image-20200513110743431](img/image-20200513110743431.png)

3.3 summary_scheduler 里主要是 WorkerScheduler 和SummaryDataProcess，其中并发主要是靠 SummaryDataProcess实现，在WorkerScheduler应该有线程池的支持。

线程启动主要在_start_worker方法，启动线程时，从cache'带过来的数据，主要通过data_id传递给summary_adapter，来取数据。

![image-20200513111502568](img/image-20200513111502568.png)