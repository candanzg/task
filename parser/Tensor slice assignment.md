

# Tensor 切片取值赋值操作

| 序号 | 取值场景                                                     |                  |      |
| ---- | ------------------------------------------------------------ | ---------------- | ---- |
| 1    | 整数数组：<br />A[0:6:3, 1:5:1, 3:5:2]<br />Ar[::, ::, 0]<br />A[1]<br />A[...,2] | StridedSlice     |      |
| 2    | 逻辑判断<br />A[iou>5] 或 A[A>5]，其中A和ious的shape相同     | Greater + select |      |
| 3    | 布尔mask:<br />A[B]: B为tensor[bool],A和B的shape相同         | Select           |      |
| 4    | 花式索引：<br />A[[4,3,1,7]]                                 | GatherND         |      |
|      |                                                              |                  |      |



|      | tensorflow                                                   | MindSpore       |      |      |      |      |      |
| ---- | ------------------------------------------------------------ | --------------- | ---- | ---- | ---- | ---- | ---- |
| 取值 | gather(     params,     indices,     validate_indices=None,     name=None,     axis=0 ) | GatherV2        |      |      |      |      |      |
|      | gather_nd(     params,     indices,     name=None )          | GatherNd        |      |      |      |      |      |
|      |                                                              |                 |      |      |      |      |      |
| 赋值 | scatter_nd(     indices,     updates,     shape,     name=None ) | ScatterNd       |      |      |      |      |      |
|      | scatter_nd_update(     ref,     indices,     updates,     use_locking=True,     name=None ) | ScatterNdUpdate |      |      |      |      |      |



# 其他

## TensorFlow 张量的切片和连接

TensorFlow 提供了几个操作来分割或提取张量的部分，或者将多个张量连接在一起。

- [tf.slice](https://www.w3cschool.cn/tensorflow_python/tensorflow_python-cdj92kbd.html)
- [tf.strided_slice](https://www.w3cschool.cn/tensorflow_python/tensorflow_python-vip72mnv.html)
- [tf.split](https://www.w3cschool.cn/tensorflow_python/tensorflow_python-x1yu2mcy.html)
- [tf.tile](https://www.w3cschool.cn/tensorflow_python/tensorflow_python-k14x2nc7.html)
- [tf.pad](https://www.tensorflow.org/api_docs/python/tf/pad)
- [tf.concat](https://www.tensorflow.org/api_docs/python/tf/concat)
- [tf.stack](https://www.w3cschool.cn/tensorflow_python/tensorflow_python-36hu2mm9.html)
- [tf.parallel_stack](https://www.tensorflow.org/api_docs/python/tf/parallel_stack)
- [tf.unstack](https://www.w3cschool.cn/tensorflow_python/tensorflow_python-bsky2o7k.html)
- [tf.reverse_sequence](https://www.w3cschool.cn/tensorflow_python/tensorflow_python-xhau2ihk.html)
- [tf.reverse](https://www.w3cschool.cn/tensorflow_python/tensorflow_python-pjcm2ifr.html)
- [tf.reverse_v2](https://www.w3cschool.cn/tensorflow_python/tensorflow_python-7s9u2ikn.html)
- [tf.transpose](https://www.w3cschool.cn/tensorflow_python/tensorflow_python-z61d2no5.html)
- [tf.extract_image_patches](https://www.tensorflow.org/api_docs/python/tf/extract_image_patches)
- [tf.space_to_batch_nd](https://www.w3cschool.cn/tensorflow_python/tensorflow_python-emqk2kf4.html)
- [tf.space_to_batch](https://www.w3cschool.cn/tensorflow_python/tensorflow_python-wygr2kbg.html)
- [tf.required_space_to_batch_paddings](https://www.w3cschool.cn/tensorflow_python/tensorflow_python-jpu92ic6.html)
- [tf.batch_to_space_nd](https://www.tensorflow.org/api_docs/python/tf/batch_to_space_nd)
- [tf.batch_to_space](https://www.tensorflow.org/api_docs/python/tf/batch_to_space)
- [tf.space_to_depth](https://www.w3cschool.cn/tensorflow_python/tensorflow_python-rkfq2kf9.html)
- [tf.depth_to_space](https://www.tensorflow.org/api_docs/python/tf/depth_to_space)
- [tf.gather](https://www.w3cschool.cn/tensorflow_python/tensorflow_python-w3uo2err.html)
- [tf.gather_nd](https://www.tensorflow.org/api_docs/python/tf/gather_nd)
- [tf.unique_with_counts](https://www.w3cschool.cn/tensorflow_python/tensorflow_python-cdi62o34.html)
- [tf.scatter_nd](https://www.w3cschool.cn/tensorflow_python/tensorflow_python-led42j40.html)
- [tf.dynamic_partition](https://www.tensorflow.org/api_docs/python/tf/dynamic_partition)
- [tf.dynamic_stitch](https://www.tensorflow.org/api_docs/python/tf/dynamic_stitch)
- [tf.boolean_mask](https://www.tensorflow.org/api_docs/python/tf/boolean_mask)
- [tf.one_hot](https://www.w3cschool.cn/tensorflow_python/tensorflow_python-fh1b2fsm.html)
- [tf.sequence_mask](https://www.w3cschool.cn/tensorflow_python/tensorflow_python-2elp2jns.html)
- [tf.dequantize](https://www.tensorflow.org/api_docs/python/tf/dequantize)
- [tf.quantize_v2](https://www.w3cschool.cn/tensorflow_python/tensorflow_python-ydjo2gcv.html)
- [tf.quantized_concat](https://www.w3cschool.cn/tensorflow_python/tensorflow_python-l3x62gas.html)
- [tf.setdiff1d](https://www.w3cschool.cn/tensorflow_python/tensorflow_python-w3bd2jro.html)

