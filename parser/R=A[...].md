#### [目录]

- [语法说明](#1)
-  [举例Numpy](#2)
-  [语法解析](#3)
-  [Parser实现思路](#4)
-  [功能自测](#5)

# 切片取值符"..."

## 1、语法说明<a name="1"/>

```
A[..., 1:]

"..."表示所在维的所有元素
```

有关 Index的语法说明，可参见：<a href="https://docs.scipy.org/doc/numpy-1.17.0/reference/arrays.indexing.html">NumPy  arrays.indexing</a>

## 2、举例Numpy<a name="2"/>

```python
>>>import  numpy as np
>>>a=np.arange(60).reshape(3,4,5)
>>>a
array([[[ 0,  1,  2,  3,  4],
[ 5,  6,  7,  8,  9],
[10, 11, 12, 13, 14],
[15, 16, 17, 18, 19]],

   [[20, 21, 22, 23, 24],
    [25, 26, 27, 28, 29],
    [30, 31, 32, 33, 34],
    [35, 36, 37, 38, 39]],

   [[40, 41, 42, 43, 44],
    [45, 46, 47, 48, 49],
    [50, 51, 52, 53, 54],
    [55, 56, 57, 58, 59]]])
```

```python
>>>a[...,1:]
array([[[ 1,  2,  3,  4],
        [ 6,  7,  8,  9],
        [11, 12, 13, 14],
        [16, 17, 18, 19]],

   [[21, 22, 23, 24],
    [26, 27, 28, 29],
    [31, 32, 33, 34],
    [36, 37, 38, 39]],

   [[41, 42, 43, 44],
    [46, 47, 48, 49],
    [51, 52, 53, 54],
    [56, 57, 58, 59]]])

>>>a[,1:]
File "<stdin>", line 1
a[,1:]
^
SyntaxError: invalid syntax
```

## 3、语法解析<a name="3"/>

### c=a[::,1:]

```python
Assign(
  targets=[Name(
    id='c',
    ctx=Store())],
  value=Subscript(
    value=Name(
      id='a',
      ctx=Load()),
    slice=ExtSlice(dims=[
      Slice(             --|
        lower=None,        |-->  ::
        upper=None,        |
        step=None),      --|
      Slice(             --|
        lower=Num(n=1),    |-->  1:
        upper=None,        |
        step=None)]),    --|
    ctx=Load()))
```
### c=a[...,1:]

```python
Assign(
  targets=[Name(
    id='b',
    ctx=Store())],
  value=Subscript(
    value=Name(
      id='a',
      ctx=Load()),
    slice=ExtSlice(dims=[
      Index(value=Ellipsis()),  ------>  ...
      Slice(                    --|
        lower=Num(n=1),           | -->  1:
        upper=None,               |
        step=None)]),           --|
    ctx=Load()))
```
## 4、Parser实现思路 <a name="4" />

### 4.1、... 与 :: 对比分析

#### 场景一：[..., ::-2, ::-1]

```python
>>> import numpy as np
>>> a=np.arange(24).reshape(2,3,4)
>>> a
array([[[ 0,  1,  2,  3],
        [ 4,  5,  6,  7],
        [ 8,  9, 10, 11]],

       [[12, 13, 14, 15],
        [16, 17, 18, 19],
        [20, 21, 22, 23]]])
>>> a[..., ::-2, ::-1]
array([[[11, 10,  9,  8],
        [ 3,  2,  1,  0]],

       [[23, 22, 21, 20],
        [15, 14, 13, 12]]])


```

#### 场景二：[...] 与 [... , ]<a name="a.5.2" />

通过NumPy验证 [...]、[... , ]、[::]、[:: , ] 四种表达的效果是相同的。如下所示：

```python
>>> a[::]
array([[[ 0,  1,  2,  3],
        [ 4,  5,  6,  7],
        [ 8,  9, 10, 11]],

       [[12, 13, 14, 15],
        [16, 17, 18, 19],
        [20, 21, 22, 23]]])
>>> a[...]
array([[[ 0,  1,  2,  3],
        [ 4,  5,  6,  7],
        [ 8,  9, 10, 11]],

       [[12, 13, 14, 15],
        [16, 17, 18, 19],
        [20, 21, 22, 23]]])
>>> a[::,]
array([[[ 0,  1,  2,  3],
        [ 4,  5,  6,  7],
        [ 8,  9, 10, 11]],

       [[12, 13, 14, 15],
        [16, 17, 18, 19],
        [20, 21, 22, 23]]])
>>> a[...,]
array([[[ 0,  1,  2,  3],
        [ 4,  5,  6,  7],
        [ 8,  9, 10, 11]],

       [[12, 13, 14, 15],
        [16, 17, 18, 19],
        [20, 21, 22, 23]]])
```

#### 场景三：[::, ...] 与 [::, ... , ]<a name="a.5.3" />

通过NumPy验证[::, ...]、[::, ... , ]、[::, ::]、[::, ::, ]四种表达式的效果一样，如下图所示：

```python
>>> a[1,::]
array([[12, 13, 14, 15],
       [16, 17, 18, 19],
       [20, 21, 22, 23]])
>>> a[1,...]
array([[12, 13, 14, 15],
       [16, 17, 18, 19],
       [20, 21, 22, 23]])

>>> a[1,::,]
array([[12, 13, 14, 15],
       [16, 17, 18, 19],
       [20, 21, 22, 23]])
>>> a[1, ...,]
array([[12, 13, 14, 15],
       [16, 17, 18, 19],
       [20, 21, 22, 23]])
```

#### 场景四：[::, ::, ...]<a name="a.5.4" />

```python
>>> a[1,0:2,::]
array([[12, 13, 14, 15],
       [16, 17, 18, 19]])
>>> a[1,0:2,...]
array([[12, 13, 14, 15],
       [16, 17, 18, 19]])
```

#### 场景五：[..., 1] 与 [::, 1]

```
>>> a
<tf.Tensor: shape=(2, 3, 4), dtype=int32, numpy=
array([[[8, 5, 7, 6],
        [9, 6, 3, 3],
        [9, 3, 3, 4]],

       [[7, 4, 6, 6],
        [4, 7, 1, 6],
        [8, 5, 2, 4]]], dtype=int32)>
>>> a[...,1]
<tf.Tensor: shape=(2, 3), dtype=int32, numpy=
array([[5, 6, 3],
       [4, 7, 5]], dtype=int32)>
>>> a[::,1]
<tf.Tensor: shape=(2, 4), dtype=int32, numpy=
array([[9, 6, 3, 3],
       [4, 7, 1, 6]], dtype=int32)>
```



### 4.2、分析结论

... 可以用 :: 代替，因此**将Index(value=Ellipsis()) 采用Slice("::")来实现。**

### 4.3、代码实现如下

ast语法：

```python
slice=ExtSlice(dims=[
  Index(value=Ellipsis()),  ------>  ...
  Slice(                    --|
    lower=Num(n=1),           | -->  1:
    upper=None,               |
    step=None)]),           --|
ctx=Load())
```
代码：

  ```c++
   +++ b/mindspore/ccsrc/pipeline/parse/parse.cc
   @@ -108,6 +108,7 @@ void Parser::BuildMethodMap() {
      expr_method_map_["Subscript"] = &Parser::ParseSubscript;
      expr_method_map_["Slice"] = &Parser::ParseSlice;
      expr_method_map_["ExtSlice"] = &Parser::ParseExtSlice;
   +  expr_method_map_["Ellipsis"] = &Parser::ParseEllipsis;
      expr_method_map_["Index"] = &Parser::ParseIndex;
  ```

 ```c++
  +// process a ellipsis
  +AnfNodePtr ParseEllipsis(const FunctionBlockPtr &block, const py::object &node) {
  +  MS_LOG(DEBUG) << "Process ast Ellipsis";
  +  MS_EXCEPTION_IF_NULL(block);
  +  AnfNodePtr op_makeslice = block->MakeResolveOperation(NAMED_PRIMITIVE_MAKESLICE);
  +  AnfNodePtr start_node = NewValueNode(kNone);
  +  AnfNodePtr stop_node = NewValueNode(kNone);
  +  AnfNodePtr step_node = NewValueNode(kNone);
  +
  +  return block->func_graph()->NewCNode({op_makeslice, start_node, stop_node, step_node});
  +}
  +
 ```

## 5、功能自测<a name="a.5" />

 ```python
+class NetWorkStepNegative2(Cell):
+    def __init__(self):
+        super(NetWorkStepNegative2, self).__init__()
+        self.tensor_ret = Tensor(np.ones([6, 5, 10], np.int32))
+        self.t1 = Tensor(np.ones([6, 8, 10], np.int32))
+        self.t2 = Tensor(np.ones([8, 10], np.int32))
+
+    def construct(self, tensor):
+        ret = tensor[..., -5::, ::-1] + self.tensor_ret
+        ret1 = tensor[...] + self.t1
+        ret2 = tensor[1, ...] + self.t2
+        return ret, ret1, ret2
+
+
+def test_slice():
+    context.set_context(mode=context.GRAPH_MODE, save_graphs=True)
+    net = NetWorkStepNegative2()
+    t = Tensor(np.ones([6, 8, 10], np.int32))
+    out = net(t)
 ```

![](./image/tensorSlice1_7_validate.dot.svg)