# Tensor取值语法A[A>5]



## 功能对照(NumPy, TF)

### NumPy:

```python
>>>import numpy as np
>>>a = np.arange(60).reshape(3,4,5)
>>>a
array([[[ 0,  1,  2,  3,  4],
        [ 5,  6,  7,  8,  9],
        [10, 11, 12, 13, 14],
        [15, 16, 17, 18, 19]],
  		 [[20, 21, 22, 23, 24],
 		    [25, 26, 27, 28, 29],
 		    [30, 31, 32, 33, 34],
 		    [35, 36, 37, 38, 39]],
 		  [[40, 41, 42, 43, 44],
 		    [45, 46, 47, 48, 49],
 		    [50, 51, 52, 53, 54],
  		  [55, 56, 57, 58, 59]]])
>>>a[a>5]
array([ 6,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22,
 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39,
 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56,
 57, 58, 59])
```

### TensorFlow:

```python
>>>import tensorflow as tf
>>>b = tf.random.uniform([5,5], minval=3, maxval=10)
>>>b
<tf.Tensor: shape=(5, 5), dtype=float32, numpy=
array([[5.6058617, 8.296783 , 5.200915 , 9.433329 , 4.505975 ],
       [3.9784203, 3.3609304, 5.208446 , 6.817649 , 6.9686513],
       [3.895597 , 5.2230997, 8.029973 , 7.124934 , 9.852983 ],
       [7.8724947, 7.276484 , 9.713179 , 9.883179 , 7.132064 ],
       [4.5355415, 7.4306736, 6.654527 , 5.2917023, 6.331172 ]],
      dtype=float32)>

>>>b[b>6]
<tf.Tensor: shape=(15,), dtype=float32, numpy=
array([8.296783 , 9.433329 , 6.817649 , 6.9686513, 8.029973 , 7.124934 ,
       9.852983 , 7.8724947, 7.276484 , 9.713179 , 9.883179 , 7.132064 ,
       7.4306736, 6.654527 , 6.331172 ], dtype=float32)>
>>> c= tf.random.uniform([5,5], minval=3, maxval=10)
>>> b[c>5]
<tf.Tensor: shape=(19,), dtype=float32, numpy=
array([5.6058617, 8.296783 , 5.200915 , 4.505975 , 3.9784203, 5.208446 ,
       6.817649 , 6.9686513, 8.029973 , 7.124934 , 9.852983 , 7.8724947,
       9.713179 , 9.883179 , 7.132064 , 4.5355415, 7.4306736, 5.2917023,
       6.331172 ], dtype=float32)>
```

