# Tensor取值语法A[B]

## 功能对照(NumPy, TF)

### Numpy:

```python

>>> import numpy as np
>>> a=np.random.uniform(1,10,[5,5])
>>> a
array([[8.49445933, 3.42667136, 9.5438419 , 7.96823064, 9.56412232],
       [5.16545617, 1.89103366, 2.4517012 , 6.21900528, 5.90764505],
       [1.967477  , 5.86471939, 8.05294118, 8.10457077, 4.43675119],
       [2.4889906 , 7.64145216, 4.54573887, 4.34910778, 1.90996466],
       [8.62679115, 8.1057503 , 8.85459223, 5.83028709, 7.08626496]])
>>> b = a >5
>>> b
array([[ True, False,  True,  True,  True],
       [ True, False, False,  True,  True],
       [False,  True,  True,  True, False],
       [False,  True, False, False, False],
       [ True,  True,  True,  True,  True]])
>>> c = a[b]
>>> c
array([8.49445933, 9.5438419 , 7.96823064, 9.56412232, 5.16545617,
       6.21900528, 5.90764505, 5.86471939, 8.05294118, 8.10457077,
       7.64145216, 8.62679115, 8.1057503 , 8.85459223, 5.83028709,
       7.08626496])
>>> c.shape
(16,)

```

### TensorFlow

```python
>>> import tensorflow as tf
>>> b = tf.random.uniform([5,5], minval=3, maxval=10)
>>> b
<tf.Tensor: shape=(5, 5), dtype=float32, numpy=
array([[6.730213 , 4.1296587, 4.2019167, 8.153129 , 6.0387044],
       [8.532621 , 5.5576334, 4.648736 , 5.4845138, 6.0474443],
       [3.8441281, 3.541793 , 9.062719 , 7.690942 , 4.612159 ],
       [3.6193283, 4.8813014, 5.823103 , 7.926292 , 8.674674 ],
       [6.166856 , 7.094277 , 5.8331847, 4.909139 , 3.6068246]],
      dtype=float32)>

>>> c = b > 5
>>> c
<tf.Tensor: shape=(5, 5), dtype=bool, numpy=
array([[ True, False, False,  True,  True],
       [ True,  True, False,  True,  True],
       [False, False,  True,  True, False],
       [False, False,  True,  True,  True],
       [ True,  True,  True, False, False]])>
>>> b[c]
<tf.Tensor: shape=(15,), dtype=float32, numpy=
array([6.730213 , 8.153129 , 6.0387044, 8.532621 , 5.5576334, 5.4845138,
       6.0474443, 9.062719 , 7.690942 , 5.823103 , 7.926292 , 8.674674 ,
       6.166856 , 7.094277 , 5.8331847], dtype=float32)>
```

