#### [目录]

- [1、语法说明](#b.1)
- [2、功能对照(NumPy, TF)](#b.2)
- [2.1、NumPy](#b.2.1)
- [2.2、TensorFlow](#b.2.2)
- [3、实现流程与编码](#b.3)
- [4、测试用例](#b.4)

# Tensor赋值语法:A[A>5]=U

****

## 1、语法说明<a name="b.1"/>

```
A[A>5] = U

A.shape == B.shape

U ：Scalar 或者Tensor(size=1)
```

## 2、功能对照(NumPy, TF)<a name="b.2"/>

### 2.1、NumPy<a name="b.2.1"/>

```python
>>> import numpy as np
>>> import tensorflow as tf
>>> a=np.random.uniform(1,10,[2,3,4])
>>> a
array([[[5.96476518, 1.13408987, 1.78899261, 9.85915332],
        [3.27071778, 4.89143085, 3.22477438, 3.98302795],
        [5.20800891, 1.8399732 , 9.51685755, 3.04481427]],

       [[1.96415043, 2.58246343, 5.82704296, 2.95975943],
        [7.42284535, 2.67324619, 2.84616425, 4.62269047],
        [7.30976523, 2.74453481, 5.90689924, 5.9722796 ]]])

>>> a[a>5] = 1
>>> a
array([[[1.        , 1.13408987, 1.78899261, 1.        ],
        [3.27071778, 4.89143085, 3.22477438, 3.98302795],
        [1.        , 1.8399732 , 1.        , 3.04481427]],

       [[1.96415043, 2.58246343, 1.        , 2.95975943],
        [1.        , 2.67324619, 2.84616425, 4.62269047],
        [1.        , 2.74453481, 1.        , 1.        ]]])

>>> a=np.random.uniform(1,10,[2,3,4])
>>> a
array([[[5.54056676, 1.01909551, 9.75871988, 6.84030132],
        [9.62006057, 3.44927377, 8.18231626, 3.75209998],
        [2.12892723, 4.77098676, 7.50871283, 4.53460429]],

       [[3.77782678, 2.2997496 , 8.30936782, 5.6977699 ],
        [7.8010546 , 1.91720876, 7.16023536, 5.78268826],
        [4.51520244, 2.17247448, 5.75409236, 3.85526818]]])
>>> a[a>5] = [1,3,4]
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ValueError: NumPy boolean array indexing assignment cannot assign 3 input values to the 12 output values where the mask is true
>>>
```

### 2.2、TensorFlow<a name="b.2.2"/>

```python

>>> import tensorflow as tf
>>> a = tf.random.uniform([2,3,4], minval=1, maxval=10)
>>> a[a>5] = 0
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "/root/miniconda3/lib/python3.7/site-packages/tensorflow_core/python/framework/ops.py", line 862, in __index__
    return self._numpy().__index__()
TypeError: only integer scalar arrays can be converted to a scalar index
>>> a[a>5] = [0]
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "/root/miniconda3/lib/python3.7/site-packages/tensorflow_core/python/framework/ops.py", line 862, in __index__
    return self._numpy().__index__()
TypeError: only integer scalar arrays can be converted to a scalar index
>>>

```

## 3、实现流程与编码<a name="b.3"/>

同A[B]=U语法，同一段实现代码和相同流程。 参见：[A[B]=U](./A[B]=U.md#a.3)

## 4、测试用例<a name="b.4"/>

```python
+class NetWorkStepNegative4(Cell):
+    def __init__(self):
+        super(NetWorkStepNegative4, self).__init__()
+        self.u_tensor = Tensor([1])
+        self.u_scalar = 5
+
+    def construct(self, a):
+        a[a>8] = self.u_tensor
+        a[a>=6] = self.u_scalar
+        a[a<3] = self.u_scalar
+        a[a<=5] = self.u_tensor
+        a[a==5] = self.u_scalar
+        return a
+def test_fz2():
+    context.set_context(mode=context.GRAPH_MODE, save_graphs=True)
+    net = NetWorkStepNegative4()
+    a = np.random.uniform(1,10,[6,8])
+    Ta = Tensor(a)
+    out = net(Ta)
```

ModelDigraph.dot.svg

![](./image/A[A>5]=U_ModelDigraph.dot.svg)

7_validate.dot.svg

![](./image/A[A>5]=U_7_validate.dot.svg)

4_abstract_specialize.dot.svg

![](./image/A[A>5]=U_4_abstract_specialize.dot.svg)