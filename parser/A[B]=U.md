#### [目录]

- [1、语法说明](#a.1)
- [2、功能对照(NumPy, TF)](#a.2)
- [2.1、NumPy](#a.2.1)
- [2.2、TensorFlow](#a.2.2)
- [3、实现流程图](#a.3)
- [4、编码实现](#a.4)
- [5、测试用例](#a.5)

# Tensor赋值语法:A[B]=U

## 1、语法说明<a name="a.1"/>

```
A[B] = U

A.shape == B.shape

U ：Scalar 或者Tensor(size=1)
```



## 2、功能对照(NumPy, TF)<a name="a.2"/>

### 2.1、NumPy<a name="a.2.1"/>

```python
>>> a=np.random.uniform(1,10,[2,3,4])
>>> a
array([[[7.10020055, 4.9399266 , 4.81014638, 7.75276674],
        [5.735233  , 9.2396662 , 2.01942859, 7.72404447],
        [5.71676613, 7.42268554, 2.11467879, 7.86230681]],

       [[1.73127464, 7.30862571, 1.02439082, 9.72630937],
        [8.82150321, 8.66521331, 4.45578772, 9.83365638],
        [4.1819204 , 7.76986584, 4.33726014, 4.33282394]]])
>>> b = a > 5
>>> b
array([[[ True, False, False,  True],
        [ True,  True, False,  True],
        [ True,  True, False,  True]],

       [[False,  True, False,  True],
        [ True,  True, False,  True],
        [False,  True, False, False]]])
>>> a[b] = 0
>>> a
array([[[0.        , 4.9399266 , 4.81014638, 0.        ],
        [0.        , 0.        , 2.01942859, 0.        ],
        [0.        , 0.        , 2.11467879, 0.        ]],

       [[1.73127464, 0.        , 1.02439082, 0.        ],
        [0.        , 0.        , 4.45578772, 0.        ],
        [4.1819204 , 0.        , 4.33726014, 4.33282394]]])

>>> a=np.random.uniform(1,10,[2,3,4])
>>> b = a > 5
>>> a[b].size
13
>>> a[b] =[1,2]
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ValueError: NumPy boolean array indexing assignment cannot assign 2 input values to the 13 output values where the mask is true
>>> a[b] = a
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: NumPy boolean array indexing assignment requires a 0 or 1-dimensional input, input has 3 dimensions
>>> a[b]=[1,2,3,4,5,6,7,8,9,10,11,12,13]
>>> a
array([[[ 1.        ,  1.71124502,  4.59324146,  2.        ],
        [ 3.50661736,  3.        ,  1.4576075 ,  4.        ],
        [ 5.        ,  6.        ,  7.        ,  8.        ]],

       [[ 3.85693857,  9.        ,  3.55436683,  4.26199247],
        [ 3.47263102,  3.28277381, 10.        , 11.        ],
        [12.        ,  1.04242384,  2.50127752, 13.        ]]])
>>> a=np.random.uniform(1,10,[2,3,4])
>>> c = a < 4
>>> a[c] = [0]
>>> a
array([[[0.        , 4.90969288, 8.39241498, 8.07132747],
        [9.45297216, 0.        , 4.04776104, 7.65747382],
        [0.        , 7.55637351, 7.00451957, 5.49235801]],

       [[6.53787699, 5.5828438 , 9.48958849, 0.        ],
        [4.00778703, 5.97635108, 0.        , 7.87131949],
        [6.20724114, 0.        , 7.05129951, 0.        ]]])
>>>
```

### 2.2、TensorFlow<a name="a.2.2"/>

```python
>>> a = tf.random.uniform([2,3,4], minval=1, maxval=10)
>>> a
<tf.Tensor: shape=(2, 3, 4), dtype=float32, numpy=
array([[[9.543776 , 9.8525505, 8.08042  , 1.2032481],
        [4.781173 , 1.4975508, 1.8756211, 6.293551 ],
        [8.578996 , 8.624415 , 4.839194 , 3.1138673]],

       [[3.5145323, 9.97074  , 6.0251164, 7.4467764],
        [6.850889 , 7.725065 , 3.4111996, 1.6470035],
        [5.3267612, 6.8387423, 8.321965 , 3.7439728]]], dtype=float32)>
>>> b = a > 5
>>> b
<tf.Tensor: shape=(2, 3, 4), dtype=bool, numpy=
array([[[ True,  True,  True, False],
        [False, False, False,  True],
        [ True,  True, False, False]],

       [[False,  True,  True,  True],
        [ True,  True, False, False],
        [ True,  True,  True, False]]])>
>>> a[b] = 0
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "/root/miniconda3/lib/python3.7/site-packages/tensorflow_core/python/framework/ops.py", line 862, in __index__
    return self._numpy().__index__()
TypeError: only integer scalar arrays can be converted to a scalar index
>>> a[b] = a
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "/root/miniconda3/lib/python3.7/site-packages/tensorflow_core/python/framework/ops.py", line 862, in __index__
    return self._numpy().__index__()
TypeError: only integer scalar arrays can be converted to a scalar index
>>>


```

## 3、实现流程图<a name="a.3"/>

![](./image/A[B]_active1.jpg)

## 4、编码实现<a name="a.4"/>

```python
// 主要代码：mindspore/ops/composite/multitype_ops/setitem_impl.py
+from ...composite import base
+from ... import functional as F
+
+setitem = base.MultitypeFuncGraph('setitem')
+
+class _TensorAssign(base.TensorAssign_):
+    def __init__(self, name):
+        base.TensorAssign_.__init__(self, name)
+
+    def __call__(self, *args):
+        pass
+
+
+_tensor_assign = _TensorAssign('tensor_assign')
+
+@setitem.register("Tensor", "Tensor", "Tensor")
+def _tensor_setitem_by_tensor(data, bool_tensor, value_tensor):
+    return _tensor_assign(data, bool_tensor, value_tensor)
+
+
+@setitem.register("Tensor", "Tensor", "Number")
+def _tensor_setitem_by_tensor(data, bool_tensor, value_tensor):
+    return _tensor_assign(data, bool_tensor, value_tensor)

```



```c++
// 主要代码： mindspore/ccsrc/operator/composite/composite.cc
+FuncGraphPtr TensorAssign::GenerateFuncGraph(const AbstractBasePtrList& args_spec_list) {
+  const std::string op_name = std::string("TensorAssign");
+  abstract::CheckArgsSize(op_name, args_spec_list, 3);
+  AbstractTensorPtr tensorPtr = abstract::CheckArg<AbstractTensor>(op_name, args_spec_list, 0);
+  AbstractTensorPtr conditionPtr = abstract::CheckArg<AbstractTensor>(op_name, args_spec_list, 1);
+  auto tensor_shape = tensorPtr->shape()->shape();
+  auto condition_shape = conditionPtr->shape()->shape();
+  if (tensor_shape != condition_shape) {
+    MS_LOG(EXCEPTION) << "The shape of the first and second arguments should be the same.";
+  }
+  FuncGraphPtr ret_graph = std::make_shared<FuncGraph>();
+  ret_graph->set_flags(FUNC_GRAPH_FLAG_CORE, true);
+  AnfNodePtr tensor_node = ret_graph->add_parameter();
+  AnfNodePtr condition_node = ret_graph->add_parameter();
+  AnfNodePtr value_node = ret_graph->add_parameter();
+  AnfNodePtr u_node = nullptr;
+  if (args_spec_list[2]->isa<AbstractScalar>()) {
+    auto PrimDtypeClass = prim::GetPythonOps("dtype", "mindspore.ops.functional");
+    auto PrimDtype = ret_graph->NewCNode({NewValueNode(PrimDtypeClass), tensor_node});
+    auto PrimShapeClass = prim::GetPythonOps("shape", "mindspore.ops.functional");
+    auto PrimShape = ret_graph->NewCNode({NewValueNode(PrimShapeClass), tensor_node});
+    auto PrimFillClass = prim::GetPythonOps("fill", "mindspore.ops.functional");
+    u_node = ret_graph->NewCNode({NewValueNode(PrimFillClass), PrimDtype, PrimShape, value_node});
+  } else if (args_spec_list[2]->isa<AbstractTensor>()) {
+    AbstractTensorPtr valuePtr = abstract::CheckArg<AbstractTensor>(op_name, args_spec_list, 2);
+    auto value_shape = valuePtr->shape()->shape();
+    if (value_shape.size() != 1 || value_shape[0] != 1) {
+      MS_LOG(EXCEPTION) << "The third argument is tensor, so its size must be 1.";
+    }
+    auto va = valuePtr->element();
+    MS_EXCEPTION_IF_NULL(va);
+    auto PrimOnesLikeClass = prim::GetPythonOps("OnesLike", "mindspore.ops.operations");
+    auto PrimOnesLike = ret_graph->NewCNode({NewValueNode(PrimOnesLikeClass)});
+    auto ones_like_node = ret_graph->NewCNode({PrimOnesLike, tensor_node});
+
+    auto PrimDtype = prim::GetPythonOps("dtype", "mindspore.ops.functional");
+    auto dtype_node = ret_graph->NewCNode({NewValueNode(PrimDtype), tensor_node});
+    auto PrimCast = prim::GetPythonOps("cast", "mindspore.ops.functional");
+    auto cast_node = ret_graph->NewCNode({NewValueNode(PrimCast), value_node, dtype_node});
+
+    auto PrimMatMulClass = prim::GetPythonOps("Mul", "mindspore.ops.operations");
+    auto PrimMatMul = ret_graph->NewCNode({NewValueNode(PrimMatMulClass)});
+    u_node = ret_graph->NewCNode({PrimMatMul, ones_like_node, cast_node});
+  } else {
+    MS_LOG(EXCEPTION) << "The third argument should be a scalar or tensor.";
+  }
+
+  MS_EXCEPTION_IF_NULL(u_node);
+  auto PrimSelectClass = prim::GetPythonOps("Select", "mindspore.ops.operations");
+  auto PrimSelect = ret_graph->NewCNode({NewValueNode(PrimSelectClass)});
+  ret_graph->set_output(ret_graph->NewCNode({PrimSelect, condition_node, u_node, tensor_node}));
+  return ret_graph;
+}
```

## 5、测试用例<a name="a.5"/>

```python
+class NetWorkStepNegative3(Cell):
+    def __init__(self):
+        super(NetWorkStepNegative3, self).__init__()
+        self.u_tensor = Tensor([1])
+        self.u_scalar = 5
+
+    def construct(self, a, b, c):
+        a[b] = self.u_tensor
+        a[c] = self.u_scalar
+        return a
+
+def test_fz():
+    context.set_context(mode=context.GRAPH_MODE, save_graphs=True)
+    net = NetWorkStepNegative3()
+    a = np.random.uniform(1,10,[2,3])
+    b = a > 5
+    c = a < 3
+    Ta = Tensor(a)
+    Tb = Tensor(b)
+    Tc = Tensor(c)
+    out = net(Ta, Tb, Tc)
```

4_abstract_specialize.dot.svg

![4_abstract_specialize.dot.svg](./image/A[B]=U_4_abstract_specialize.dot.svg)

7_validate.dot.svg

![7_validate.dot.svg](./image/A[B]=U_7_validate.dot.svg)