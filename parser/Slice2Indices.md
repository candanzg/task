[TOC]

# 实现Tensor切片赋值方案

## 1、设计方案

### 1.1、约定：

A->Tensor

S-> slice表达式（比如“:3,::, 1::2”等）

U-> 更新的valueTensor。 Maybe  A.shape != U.shape

### 1.2、目标

A[S] = U

ScatterNdUpdate的第一个参数（ref) 是个Parameter。加入A是外部输入的可以用此算子，若A是计算过程中生成的Tensor，则用以下流程好

### 1.3、实现思路

1. S->indices
   <a href="#indices">参见:Convert Slice to indices</a>

   注意，要将S展开，比如[:2, ::] -> [0:2, 0:M:1]

   

2. 广播 U，同A.shape

   注：转换成（n,1)，计算完后，再reshape(A.shape)

   Indices = indices.reshape(-1,1)

   U_1D = ms.flatten(U)

   A_size = ms.Size(A)

   A_1DShape = （A_size() , )

   U_1D_braodcast = ms.ScatterNd(Indices, U_1D, A_1DShape)

   U_braodcast = ms.Reshpae(U_1D_braodcast, A.shape)

3. 计算Condition

   1. 方案一：通过执行python代码生成

      1. 构造B[A.shape] ->bool
      2. B[S] = True
      3. Condition = B

   2. 方案二：通过indices生成

      1. U_bool = ms.Fill(ms.bool, U.shap, True)
         U_size = ms.Size(U)

         U_1DBool = ms.Reshape(U_bool, (U_size, ))

      2. Indices = indices.reshape(-1,1)

      3. A_size = ms.Size(A)

         A_1DShape = （A_size() , )

      4. B = ms.ScatterNd(indices, U_1DBool, A_1DSahpe)

      5. Condition = ms.Reshape(B, ms.Shape(A)) 

4. Select

   1. ms.Select(Condition, U_broadcast, A)

## 2、Convert Slice to indices <a name="indices"></a>

**注意，要将S展开，比如[:2, ::] -> [0:2, 0:M:1]**

参加以下各种 S 表达场景

example: np.ravel_multi_index((np.ix_(np.r_[0:3],[1,2])),(3,3))

```python
import numpy as np
a=np.arange(60).reshape(3,4,5)
print(a)
[[[ 0  1  2  3  4]
[ 5  6  7  8  9]
[10 11 12 13 14]
[15 16 17 18 19]]

 [[20 21 22 23 24]
  [25 26 27 28 29]
  [30 31 32 33 34]
  [35 36 37 38 39]]

 [[40 41 42 43 44]
  [45 46 47 48 49]
  [50 51 52 53 54]
  [55 56 57 58 59]]]
```

### a[0:3:1, 0:4:2, 1:5:3]

```python
>>>a[0:3:1, 0:4:2, 1:5:3]
[[[ 1  4]
  [11 14]]

 [[21 24]
  [31 34]]

 [[41 44]
  [51 54]]]
```



```python
>>>np.ravel_multi_index((np.ix(np.r[0:3:1],np.r[0:4:2],np.r[1:5:3])), a.shape)
array([[[ 1,  4],
[11, 14]],

   [[21, 24],
    [31, 34]],

   [[41, 44],
    [51, 54]]])
```

### a[::1, 1, ::2]

```python
>>>a[::1, 1, ::2]
array([[ 5,  7,  9],
       [25, 27, 29],
       [45, 47, 49]])
```

```python
>>>np.ravel_multi_index((np.ix_(np.r_[::1],[1],np.r_[::2])), a.shape)
array([], shape=(0, 1, 0), dtype=int64)
>>>np.ravel_multi_index((np.ix_(np.r_[0:3:1],[1],np.r_[::2])), a.shape)
array([], shape=(3, 1, 0), dtype=int64)
>>>np.ravel_multi_index((np.ix_(np.r_[0:3:1],[1],np.r_[0:5:2])), a.shape)
array([[[ 5,  7,  9]],

   [[25, 27, 29]],

   [[45, 47, 49]]])
```

### a[::,::,0]

```python
>>>a[::,::,0]
array([[ 0,  5, 10, 15],
       [20, 25, 30, 35],
       [40, 45, 50, 55]])
```

```python
>>>np.ravel_multi_index((np.ix_(::,::,0)), a.shape)
  File "<stdin>", line 1
    np.ravel_multi_index((np.ix_(::,::,0)), a.shape)
                                 ^
SyntaxError: invalid syntax

>>>np.ravel_multi_index((np.ix_(np.r_[0:3:1],np.r_[0:4:1],np.r_[0])), a.shape)
array([[[ 0],
[ 5],
[10],
[15]],

   [[20],
    [25],
    [30],
    [35]],

   [[40],
    [45],
    [50],
    [55]]])

>>>np.ravel_multi_index((np.ix_(np.r_[0:3:1],np.r_[0:4:1],[0])), a.shape)
array([[[ 0],
[ 5],
[10],
[15]],

   [[20],
    [25],
    [30],
    [35]],

   [[40],
    [45],
    [50],
    [55]]])
```

### a[2,3,4]

```
>>>a[2,3,4]
59
```

```
>>>np.ravel_multi_index((np.ix_([2],[3],[4])), a.shape)
array([[[59]]]
```

### a[1]

```
>>>a[1]
array([[20, 21, 22, 23, 24],
       [25, 26, 27, 28, 29],
       [30, 31, 32, 33, 34],
       [35, 36, 37, 38, 39]])
```

```
>>>np.ravel_multi_index((np.ix_([1],np.r_[0:4:1],np.r_[0:5:1])), a.shape)
array([[[20, 21, 22, 23, 24],
        [25, 26, 27, 28, 29],
        [30, 31, 32, 33, 34],
        [35, 36, 37, 38, 39]]])
```

